import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import Toast from "react-native-toast-message";
import AuthProvider from "./contexts/auth/store/Auth";
// import TopBarNavigation from "./Navigators/TopBarNavigation";
import BottomBarNavigation from './Navigators/BottomTabNavigation';
// import StackNavigator from './Navigators/StackNavigation';
// import DrawerNavigation from './Navigators/DrawerNavigation';
// import RouteParamsNavigation from './Navigators/RouteParamsNavigator';

export default function App() {
    return (
        <AuthProvider>
        <SafeAreaProvider>
            <BottomBarNavigation />
            <Toast ref={(ref) => Toast.setRef(ref)} />
        </SafeAreaProvider>
        </AuthProvider>
    );
}