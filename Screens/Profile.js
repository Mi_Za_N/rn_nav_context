import React, { useContext, useState, useCallback } from 'react';
import { View, Text, StyleSheet, Button,ScrollView } from "react-native";
import { useFocusEffect } from "@react-navigation/native"
import axios  from 'axios';
import AuthGlobal from "../contexts/auth/store/AuthGlobal";
import AsyncStorage from '@react-native-async-storage/async-storage';
import baseURL from '../BaseUrl';
import { logoutUser } from "../contexts/auth/actions/Auth.actions"

const Profile = (props) => {
  const context = useContext(AuthGlobal)
  const data = context.stateUser.userProfile;
  const [userProfile, setUserProfile] = useState()

    useFocusEffect(
        useCallback(() => {
        if (
            context.stateUser.isAuthenticated === false || 
            context.stateUser.isAuthenticated === null
        ) {
            props.navigation.navigate("Login")
        }

        AsyncStorage.getItem("jwt")
            .then((res) => {
                axios
                    .get(`${baseURL}users/${context.stateUser.user.userId}`, {
                      
                        headers: { Authorization: `Bearer ${res}` },
                    })
                    .then((user) => setUserProfile(user.data))
            })
            .catch((error) => console.log(error))

        return () => {
            setUserProfile();
        }

    }, [context.stateUser.isAuthenticated]))


  return (
    <ScrollView contentContainerStyle={styles.subContainer}>
        <Text style={{ fontSize: 30 }}>
            {data ? data.name : "" }
        </Text>
        <View style={{ marginTop: 20 }}>
            <Text style={{ margin: 10 }}>
                Email: {data ? data.email : ""}
            </Text>
            <Text style={{ margin: 10 }}>
                Phone: {data ? data.phone : ""}
            </Text>
        </View>
        <View style={{ marginTop: 80 }}>
            <Button title={"Sign Out"} onPress={() => [
                AsyncStorage.removeItem("jwt"),
                logoutUser(context.dispatch)
            ]}/>
        </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "#48d969"
  },
  text: {
    fontSize: 20,
    color: "#ffffff",
    fontWeight: "800",
  },
});
export default Profile