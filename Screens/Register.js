import React ,{useState} from "react"
import { 
    View, 
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
    ScrollView
} from "react-native";
import baseURL from "../BaseUrl";
import axios  from "axios";
import Input from "../components/UI/Input";
import EasyButton from "../components/UI/EasyButton";
import FormContainer from "../components/UI/FormContainer";
import Icon from "react-native-vector-icons/FontAwesome"
import Toast from "react-native-toast-message";
import Colors from "../constants/Colors";


const Register = (props) => {

   const [name, setName] = useState("Mizan Rahman");
   const [email, setEmail] = useState("mizan.softdev@gmail.com");
   const [phone, setPhone] = useState("01788595619");
   const [password, setPassword] = useState("12255889");
   const [error, setError] = useState("");




  const handleSubmit = () => {
    if (email === "" || name === "" || phone === "" || password === "") {
      setError("Please fill in the form correctly");
    }

    let user = {
      name: name,
      email: email,
      password: password,
      phone: phone,
      isAdmin: true,
    };
    // console.log(user); 
    axios.post(`${baseURL}users/register`, user)
      .then((res) => {
        if (res.status == 200) {
          Toast.show({
            topOffset: 60,
            type: "success",
            text1: "Registration Succeeded",
            text2: "Please Login into your account",
          });
          setTimeout(() => {
            props.navigation.navigate("Login");
          }, 500);
        }
      })
      .catch((error) => {
        Toast.show({
          topOffset: 60,
          type: "error",
          text1: "Something went wrong",
          text2: "Please try again",
        });
      });
  };

    //  const handleSubmit =  () => {
    // props.navigation.navigate("otp");
    //     Toast.show({
    //         topOffset: 60,
    //         type: "success",
    //         text1: "Regitration Success",
    //         });
    // };

    return (
      <ScrollView style={{paddingTop: 50}}>
       <FormContainer title="Register">
           <View style={styles.label}>
            <Text style={{paddingLeft: 0, fontWeight: "bold", fontSize: 20}}>Register</Text>
            </View>
      <View style={styles.row}>
        <View style={styles.inputWrap}>
          {/* <Text style={styles.label}>First Name</Text> */}
                <Input
                  placeholder={"First Name"}
                  name={"Name"}
                  id={"name"}
                  value={"Mizan"}
                  // onChangeText={(text) => setEmail(text.toLowerCase())}
                />
        </View>

        <View style={styles.inputWrap}>
          <Input
            placeholder={"Last Name"}
            name={"Name"}
            id={"name"}
            value={"Rahman"}
            // onChangeText={(text) => setEmail(text.toLowerCase())}
          />
        </View>
      </View>
           {/* <Text style={styles.label}>Last Name</Text> */}
           <Input 
            placeholder="Name"
            name="Name"
            id="Name"
            value={name}
            onChangeText={(text) => setName(text)}
           />
           {/* <Text style={styles.label}>Last Name</Text> */}
           <Input 
            placeholder="Email"
            name="email"
            id="email"
            value={email}
            onChangeText={(text) => setEmail(text)}
           />
            {/* <View style={styles.label}>
               <Text style={{ textDecorationLine: "underline"}}>Phone</Text>
           </View> */}
           
           <Input 
            placeholder="Phone Number"
            name="phone"
            id="phone"
            value={phone}
            keyboardType={"numeric"}
            onChangeText={(text) => setPhone(text)}
           />
           {/* <View style={styles.label}>
             <Text style={{ textDecorationLine: "underline"}}>Password</Text>
           </View> */}
         <Input
            placeholder={"Enter Password"}
            name={"password"}
            id={"password"}
            secureTextEntry={true}
            value={password}
            onChangeText={(text) => setPassword(text)}
          />

           <View style={styles.buttonContainer}>
               <EasyButton
                large
                primary
               onPress={handleSubmit}               
               >
                   <Text style={styles.buttonText}>Confirm</Text>
               </EasyButton>
          <TouchableOpacity 
              onPress={() => props.navigation.navigate("Login")}
              >
            <Text style={styles.middleText}>Don't have an account yet?
              <Text style={{ marginTop: 40, color: "#DD502C"}}>Login</Text>
            </Text>
    
          </TouchableOpacity>
           </View>

       </FormContainer>
       </ScrollView>
    )
}

const styles = StyleSheet.create({
    label: {
        width: "90%",
        marginTop: 20
    },
    buttonContainer: {
        width: "80%",
        marginBottom: 80,
        marginTop: 5,
        alignItems: "center"
    },
    buttonText: {
       color: "white",
        fontWeight: "bold",
        fontSize: 16
    },
    imageContainer: {
        width: 120,
        height: 120,
        borderStyle: "solid",
        borderWidth: 8,
        padding: 0,
        justifyContent: "center",
        borderRadius: 100,
        borderColor: "#E0E0E0",
        elevation: 10
    },
    image: {
        width: "100%",
        height: "100%",
        borderRadius: 100
    },
    imagePicker: {
        position: "absolute",
        right: 5,
        bottom: 5,
        backgroundColor: Colors.secondary,
        padding: 8,
        borderRadius: 100,
        elevation: 20
    },
 row: {
    flex: 1,
    flexDirection: "row",
    width: "90%"
  },
  inputWrap: {
    flex: 1,
    borderColor: "#000",
    // borderBottomWidth: 1,
    // marginBottom: 2
  },
  inputdate: {
    fontSize: 14,
    marginBottom: -12,
    color: "#6a4595"
  },
  buttonGroup: {
    width: "80%",
    alignItems: "center",
  },
 middleText: {
    marginBottom: 20,
    alignSelf: "center",
  },
  inputcvv: {
    fontSize: 14,
    marginBottom: -12,
    color: "#6a4595"
  }
})
export default Register;